// @flow
/* eslint eqeqeq: "off" */

import ReactDOM from 'react-dom';
import * as React from 'react';
import { Component } from 'react-simplified';
import { HashRouter, Route, NavLink } from 'react-router-dom';
import { Alert, NavBar, Card, ListGroup, Button } from './widgets';

import createHashHistory from 'history/createHashHistory';
const history = createHashHistory(); // Use history.push(...) to programmatically change path, for instance after successfully saving a student

class Case {
  id: number;
  static nextId = 1;
  overskrift: string;
  content: string;
  image: string;
  comments: string[];

  constructor(overskrift: string, content: string, image: string) {
    this.id = Case.nextId++;
    this.overskrift = overskrift;
    this.content = content;
    this.image = image;
    this.comments = new Array(10);
  }

  comment(c: string) {
    this.comments.push(c);
  }
}
let cases = [
  new Case('Title1', 'hå', 'https://i.imgur.com/vSD5rT8.gif'),
  new Case('Title 2', 'Hå', 'http://s1.bwallpapers.com/wallpapers/2014/01/29/snow-cat_045842554.jpg'),
  new Case(
    'Title 2',
    'Hå',
    'https://proxy.duckduckgo.com/iu/?u=http%3A%2F%2Fwww.gordonrigg.com%2Fthe-hub%2Fwp-content%2Fuploads%2F2015%2F06%2Fcats.jpg&f=1'
  ),
  new Case('Title 2', 'Hå', 'http://hdgifs.com/wp-content/uploads/2017/04/pikaaaaa-chuuuuuuuu.gif'),
  new Case('Title 2', 'Hå', 'http://apod.nasa.gov/apod/image/1704/ManDogSun_Hackmann_1600.jpg'),
  new Case('Title 2', 'Hå', 'http://apod.nasa.gov/apod/image/1704/ManDogSun_Hackmann_1600.jpg'),
  new Case('Title 2', 'Hå', 'http://apod.nasa.gov/apod/image/1704/ManDogSun_Hackmann_1600.jpg'),
  new Case('Title 2', 'Hå', 'http://apod.nasa.gov/apod/image/1704/ManDogSun_Hackmann_1600.jpg'),
  new Case('Title 2', 'Hå', 'http://apod.nasa.gov/apod/image/1704/ManDogSun_Hackmann_1600.jpg'),
  new Case('Title 2', 'Hå', 'http://apod.nasa.gov/apod/image/1704/ManDogSun_Hackmann_1600.jpg'),
  new Case('Title 2', 'Hå', 'http://apod.nasa.gov/apod/image/1704/ManDogSun_Hackmann_1600.jpg'),
  new Case('Title 2', 'Hå', 'http://apod.nasa.gov/apod/image/1704/ManDogSun_Hackmann_1600.jpg'),
  new Case('Title 2', 'Hå', 'http://apod.nasa.gov/apod/image/1704/ManDogSun_Hackmann_1600.jpg'),
  new Case('Title 2', 'Hå', 'http://apod.nasa.gov/apod/image/1704/ManDogSun_Hackmann_1600.jpg'),
  new Case('Title 2', 'Hå', 'http://apod.nasa.gov/apod/image/1704/ManDogSun_Hackmann_1600.jpg'),
  new Case('Title 2', 'Hå', 'http://apod.nasa.gov/apod/image/1704/ManDogSun_Hackmann_1600.jpg')
];

cases[0].comment('Veldig Bra');
cases[0].comment('xD');

class Menu extends Component {
  render() {
    return (
      <NavBar>
        <NavBar.Brand>Public Paper</NavBar.Brand>
        <NavBar.Link to="/newCase">New Case 💘 </NavBar.Link>
        <NavBar.Link to="/tags">Tags</NavBar.Link>
      </NavBar>
    );
  }
}

class Home extends Component {
  render() {
    return (
      <div>
        {cases.map(e => (
          <Card className="cc-size" title={e.overskrift}>
            <ListGroup.Item>
              <img className="card-img-top" src={e.image} src={e.image} alt={e.image} />
            </ListGroup.Item>
            <ListGroup.Item to={'/case/' + e.id}>Comments</ListGroup.Item>
          </Card>
        ))}
      </div>
    );
  }
}

class CaseDetails extends Component<{ match: { params: { id: number } } }> {
  render() {
    let e = cases.find(e => e.id == this.props.match.params.id);
    if (!e) {
      Alert.danger('Case not found: ' + this.props.match.params.id);
      return null; // Return empty object (nothing to render)
    }
    return (
      <ListGroup>
        <Card title={e.overskrift}>
          <ListGroup.Item>
            <img className="card-img-top" src={e.image} alt="Image Cap" />
          </ListGroup.Item>
          <ListGroup.Item>{e.content}</ListGroup.Item>
        </Card>
        <Card title="Comments">
          <ListGroup>
            {e.comments.map(comment => (
              <ListGroup.Item>{comment}</ListGroup.Item>
            ))}
            <ListGroup.Item>
              <Button.Light onClick={this.comment}>Comment</Button.Light>
            </ListGroup.Item>
          </ListGroup>
        </Card>
      </ListGroup>
    );
  }

  comment() {
    history.push('/case/' + this.props.match.params.id + '/comment');
  }
}

class newComment extends Component<{ match: { params: { id: number } } }> {
  comment = '';
  render() {
    return (
      <Card title="New Comment">
        <form>
          <ListGroup>
            <ListGroup.Item>
              <input
                type="text"
                value={this.comment}
                onChange={(event: SyntheticInputEvent<HTMLInputElement>) => (this.comment = event.target.value)}
              />
            </ListGroup.Item>
            <ListGroup.Item>
              <Button.Success onClick={this.save}>Comment</Button.Success>
              <Button.Danger onClick={this.discard}>Discard</Button.Danger>
            </ListGroup.Item>
          </ListGroup>
        </form>
      </Card>
    );
  }

  save() {
    let e = cases.find(e => e.id == this.props.match.params.id);
    if (!e) {
      Alert.danger('Case not found: ' + this.props.match.params.id);
      return null; // Return empty object (nothing to render)
    }
    e.comment(this.comment);
    history.push('/case/' + this.props.match.params.id);
  }

  discard() {
    history.push('/case/' + this.props.match.params.id);
  }
}

class CasetAdd extends Component {
  overskrift = '';
  content = '';
  image = '';

  render() {
    return (
      <Card title="New Student">
        <form>
          <ListGroup>
            <ListGroup.Item>
              Title:{' '}
              <input
                type="text"
                value={this.overskrift}
                onChange={(event: SyntheticInputEvent<HTMLInputElement>) => (this.overskrift = event.target.value)}
              />
            </ListGroup.Item>
            <ListGroup.Item>
              Image Link:{' '}
              <input
                type="text"
                value={this.image}
                onChange={(event: SyntheticInputEvent<HTMLInputElement>) => (this.image = event.target.value)}
              />
            </ListGroup.Item>
            <ListGroup.Item>
              text:{' '}
              <input
                type="text"
                value={this.content}
                onChange={(event: SyntheticInputEvent<HTMLInputElement>) => (this.content = event.target.value)}
              />
            </ListGroup.Item>
            <ListGroup.Item>
              <Button.Success onClick={this.save}>Register</Button.Success>
              <Button.Danger onClick={this.discard}>Discard</Button.Danger>
            </ListGroup.Item>
          </ListGroup>
        </form>
      </Card>
    );
  }

  save() {
    let c = new Case(this.overskrift, this.content, this.image);
    cases.push(c);
    // Go to StudentDetails after successful save
    history.push('/');
  }

  discard() {
    history.push('/');
  }
}

const root = document.getElementById('root');
if (root)
  ReactDOM.render(
    <HashRouter>
      <div>
        <Alert />
        <Menu />
        <Route exact path="/" component={Home} />
        <Route exact path="/case/:id" component={CaseDetails} />
        <Route exact path="/case/:id/comment" component={newComment} />
        <Route exact path="/newCase" component={CasetAdd} />
      </div>
    </HashRouter>,
    root
  );
