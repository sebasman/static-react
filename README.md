# React example with component state

## Prerequisites

- Flow and Prettier installed globally:

```sh
npm install -g flow-bin prettier
```

- Editor with Flow and prettier support

## Fetch, install dependencies, and run

```sh
git clone https://gitlab.stud.idi.ntnu.no/sebasman/static-react.git
cd static-react
npm install
npm start
```
